#ifndef __Utility_cpp__NS__
#define __Utility_cpp__NS__

#include <cmath>
#include <random>
#include <vector>
#include <utility>
#include <iostream>

namespace ns {
  // pi costant
  template<class Real>
  constexpr Real pi = Real(3.1415926535897932385L);

  template<class Real>
  constexpr Real e = Real(2.71828182845904523536L);

  template<class Real>
  constexpr Real inv_e = Real(1.)/e<Real>;

  // Compute the of the sum of a and b, supposing that a and b are log too.
  // The effective math operation is:
  // log(e^a+e^b)
  // The operation is done in a way that can't produce inf as result.
  template <typename Real>
  inline Real log_sum(const Real a, const Real b) {
    if (a > b) {
      return a + log(Real(1.)+exp(b-a));
    } else {
      return b + log(Real(1.)+exp(a-b));
    }
  }

  // The same as above, but now it's a difference.
  // Must be a > b
  // The effective math operation is:
  // log(e^a-e^b)
  template <typename Real>
  inline Real log_diff(const Real a, const Real b) {
    return a + log(Real(1.)-exp(b-a));
  }

  // Convert a sample from U[0,1] to Beta(alpha, 1) distribution
  template <typename Real>
  inline Real uniform2beta_alpha_one(std::size_t alpha, Real uniform_sample) {
    return pow(uniform_sample, Real(1.)/Real(alpha));
  }

  // Convert a sample from U[0,1] to the logarithm of a to Beta(alpha, 1) distribution.
  template <typename Real>
  inline Real uniform2log_beta_alpha_one(std::size_t alpha, Real uniform_sample) {
    return log(uniform_sample) / Real(alpha);
  }

  // Compute Montecarlo Mean and Variance using a vector of indipendent samples
  template <typename Real>
  inline std::pair<Real, Real> montecarlo_estimation(std::vector<Real>& samples) {
    Real sum = Real(0.);
    Real sum_sq = Real(0.);
    for (Real& v: samples) {
      sum += v;
      sum_sq += v*v;
    }
    Real n_samples = Real(samples.size());
    Real mean = sum / n_samples;
    Real variance = (sum_sq/(n_samples) - mean*mean)/(n_samples-Real(1.));
    return std::make_pair(mean, sqrt(variance));
  }

  // L2 Norm of a vector
  template <class Vector, class Real = typename Vector::value_type>
  inline Real vector_norm(const Vector& v) {
    Real norm = Real(0.);
    for (auto& x: v) {
      norm += x*x;
    }
    return std::sqrt(norm);
  }

  // Volume of a n-simension hypersphere
  template <typename Real>
  Real hypersphere_log_volume(std::size_t n, Real radius) {
    Real gamma = Real(0.);
    if (n % 2 == 0) {
      for (unsigned i = 2; i <= n/2; i++) {
        gamma += std::log(Real(i));
      }
    } else {
      for (unsigned i = 1; i <= n; i+=2) {
        gamma += std::log(Real(i)/Real(2.));
      }
      gamma += Real(0.5) * std::log(pi<Real>);
    }

    Real dim = Real(n);
    return dim / Real(2.) * std::log(pi<Real>) - gamma + dim * std::log(radius); 
  }
}


#endif