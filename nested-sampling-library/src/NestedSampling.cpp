#ifndef __NestedSampling_cpp__NS__
#define __NestedSampling_cpp__NS__

#include <Utility.cpp>

#include <iostream>
#include <fstream>
#include <string>
#include <cstddef>    // std::size_t
#include <vector>     // std::vector
#include <queue>      // std::priority_queue
#include <functional> // std::greater
#include <utility>    // std::make_pair
#include <cmath>      // std::exp

namespace ns {

template <class Model, class Real = typename Model::Real, class Parameter = typename Model::Parameter>
class NestedSampling {
  public:
    using Sample = std::pair<Real, Parameter>;
  private:
    Model& _model;
    std::size_t _n_levels;
    std::size_t _n_particles;
    bool run_flag;
    std::priority_queue <Sample, std::vector<Sample>, std::greater<Sample>> alive_particles;
    std::vector<Sample> increasing_samples;

  protected:
    Real log_expected_X(std::size_t i) {
      return -Real(i)/Real(_n_particles);
    }
  public:

    // Setter methods
    void set_n_levels(const std::size_t n_levels) {
      _n_levels = n_levels;
    }

    void set_n_particles(const std::size_t n_particles) {
      _n_particles = n_particles;
    }

    // Contrustuctor
    NestedSampling(Model& model, const std::size_t n_levels, const std::size_t n_particles) :
      _model(model),
      _n_levels(n_levels),
      _n_particles(n_particles),
      run_flag(false)
    {
      static_assert(std::numeric_limits<Real>::is_iec559, "IEEE 754 required for Real");
    }

    void run(bool verbose = false) {
      // Check that the memory is empty
      while (not alive_particles.empty()) {
        alive_particles.pop();
      }
      increasing_samples.clear();
      
      // Initial Prior Samples
      for (std::size_t i = 1; i <= _n_particles; i++) {
        Parameter new_particle_parameter = _model.prior_sample();
        Sample new_particle = std::make_pair(_model.log_likelihood(new_particle_parameter), new_particle_parameter);
        alive_particles.push(new_particle);
        if (verbose) {
          // std::clog << "Initial particle " << i << ": " << new_particle.second << ",  L = " << new_particle.first << std::endl;
          std::clog << "Initial particle " << i << ": L = " << new_particle.first << std::endl;
        }
      }

      // Level Steps
      for (std::size_t l = 1; l <= _n_levels; l++) {
        // Storing lowest likelihood
        Sample dead_particle = alive_particles.top();
        alive_particles.pop();
        increasing_samples.push_back(dead_particle);

        if (verbose) {
          // std::clog << "Dead " << l << " at:  " << dead_particle.second << ", " << dead_particle.first << std::endl;
          std::clog << "Dead " << l << " at: L = " << dead_particle.first << std::endl;
        }

        // Creating new alive particle
        Parameter new_alive_parameter = _model.constrainted_prior_sample(dead_particle);
        Sample new_alive = std::make_pair(_model.log_likelihood(new_alive_parameter), new_alive_parameter);
        alive_particles.push(new_alive);

      }

      run_flag = true;
    }

    Real information(Real Z) {
      Real H = Real(0.);

      for (std::size_t l = 1; l <= _n_levels; l++) {
        H += exp(increasing_samples[l-1].first + log_diff(log_expected_X(l-1), log_expected_X(l))) * (increasing_samples[l-1].first - log(Z));
      }
      H /= Z;

      return H;
    }

    std::pair<Real, Real> crude_log_evidence() {
      if (not run_flag) {
        std::cerr << "Fatal error: crude_log_evidence() called before run()" << std::endl;
        exit(1);
      }

      Real logZ = Real(-std::numeric_limits<Real>::infinity());
      for (std::size_t l = 1; l <= _n_levels; l++) {
        Real logBin = log_diff(log_expected_X(l-1), log_expected_X(l)) + increasing_samples[l-1].first;
        logZ = log_sum(logZ, logBin);
      }

      Real Z = exp(logZ);
      //std::clog << Z << std::endl;
      Real H = information(Z);
      return std::make_pair(logZ, sqrt(H/Real(_n_particles)));
    }

    template <class RandomGenerator>
    std::pair<Real, Real> montecarlo_log_evidence(std::size_t mc_samples,RandomGenerator& rng) {
      if (not run_flag) {
        std::cerr << "Fatal error: montecarlo_log_evidence() called before run()" << std::endl;
        exit(1);
      }

      std::uniform_real_distribution<Real> unitary_uniform(Real(0.), Real(1.));

      std::vector<Real> logZ(mc_samples);
      for (auto& e: logZ) {
        e = Real(-std::numeric_limits<Real>::infinity());
        Real log_lastX = Real(0.);
        for (std::size_t l = 1; l <= _n_levels; l++) {
          // std::clog << "LastX " << l << ": " << std::exp(log_lastX) << std::endl;
          Real log_nextX = uniform2log_beta_alpha_one(_n_particles, unitary_uniform(rng)) + log_lastX;
          Real logBin = log_diff(log_lastX, log_nextX) + increasing_samples[l-1].first;
          // std::clog << "logBin " << l << ": " << logBin << std::endl;
          e = log_sum(e, logBin);
          log_lastX = log_nextX;
        }
        //std::clog << e << std::endl;
      }

      return montecarlo_estimation(logZ);
    }

    Real crude_information() {
      if (not run_flag) {
        std::cerr << "Fatal error: crude_information() called before run()" << std::endl;
        exit(1);
      }
      Real Z = exp(crude_log_evidence());
      Real H = Real(0.);
      for (std::size_t l = 1; l <= _n_levels; l++) {
        H += exp(log_diff(log_expected_X(l-1), log_expected_X(l)))/Z * log(increasing_samples[l-1].first/Z);
      }
      return H;
    }

    void save_samples(std::string filename) {
      std::ofstream outfile(filename);
      for (std::size_t l = 1; l <= _n_levels; l++) {
        outfile << log_expected_X(l) << ' ' << increasing_samples[l-1].first << std::endl;
      }
      outfile.close();
    }
};


}

#endif