#ifndef __Model_cpp__NS__
#define __Model_cpp__NS__

#include <Utility.cpp>
#include <utility>      // std::pair

namespace ns {

template <typename Real_, typename Parameter_> 
class Model {
  public:
    using Real = Real_;
    using Parameter = Parameter_;
    using Sample = std::pair<Real, Parameter>;

    Model() {}

    virtual Real prior(const Parameter&) = 0;
    virtual Real log_likelihood(const Parameter&) = 0;
    virtual Parameter prior_sample() = 0;
    virtual Parameter constrainted_prior_sample(const Sample&) = 0;
};

}

#endif