#ifndef __DiffusiveNestedSampling_cpp__NS__
#define __DiffusiveNestedSampling_cpp__NS__

#include <Utility.cpp>

#include <cassert>
#include <iostream>
#include <fstream>
#include <string>
#include <cstddef>    // std::size_t
#include <vector>     // std::vector
#include <set>        // std::multiset
#include <functional> // std::greater
#include <utility>    // std::make_pair
#include <cmath>      // std::exp

namespace ns {

template <class Model, class RandomGenerator, class Real = typename Model::Real, class Parameter = typename Model::Parameter>
class DiffusiveNestedSampling {
  public:
    using Sample = std::pair<Real, Parameter>;
    using Particle = std::pair<Sample, std::size_t>;
  private:
    Model& _model;
    RandomGenerator& _rng;
    std::size_t _n_levels;
    Real _regularization;
    Real _enforce;
    Real _shrink;

    bool built_levels_flag;
    std::vector<Sample> level;
    Particle last_mc_sample;
    Real last_mc_prior;
    std::vector<Real> level_counter_after_next, conditioned_next_level_counter; // n(j), n(L>L_j|j) using article notation

    Real min_likelihood;

    std::vector<std::vector<Sample>> collected_samples_at_level;
  protected:
    inline Real X_rateo_with_next(std::size_t j) {
      if (j == _n_levels) {
        return Real(0.);
      }
      return (conditioned_next_level_counter[j] + _regularization*_shrink) / (level_counter_after_next[j] + _regularization);
    }

    inline void update_min_likelihood(const Real& l) {
      if (l < min_likelihood) {
        min_likelihood = l;
      }
    }

    inline void manage_counters_for_X_rateo(std::size_t lev, Real lik) {
      if (lev == _n_levels) {
        return;
      }
      level_counter_after_next[lev] += Real(1.);
      if (lik > level[lev+1].first) {
        conditioned_next_level_counter[lev] += Real(1.);
      }
    }

  public:
    // Contrustuctor
    DiffusiveNestedSampling(
      Model& model,
      RandomGenerator& rng,
      const std::size_t n_levels,
      Real regularization,
      Real enforce,
      Real shrink = inv_e<Real>
    ) :
      _model(model),
      _rng(rng),
      _n_levels(n_levels),
      _regularization(regularization),
      _enforce(enforce),
      _shrink(shrink),
      built_levels_flag(false),
      level(n_levels + 1),
      collected_samples_at_level(n_levels + 1),
      level_counter_after_next(n_levels + 1, Real(0.)),
      conditioned_next_level_counter(n_levels + 1, Real(0.))
    {
      static_assert(std::numeric_limits<Real>::is_iec559, "IEEE 754 required for Real");
      min_likelihood = Real(std::numeric_limits<Real>::infinity());
    }

    void build_levels(std::size_t n_likelihoods_per_level, Real backtracking, bool verbose = false) {
      // Declaring data structures
      std::vector<Sample> current_log_likelihoods;

      // 0 Likelihood level. The Parameter is fictitious
      level[0] = std::make_pair(Real(-std::numeric_limits<Real>::infinity()), Parameter());
      
      // Initial Prior Samples
      for (std::size_t i = 1; i <= n_likelihoods_per_level; i++) {
        Parameter new_sample_parameter = _model.prior_sample();
        Real new_sample_likelihood = _model.log_likelihood(new_sample_parameter);
        Sample new_sample = std::make_pair(new_sample_likelihood, new_sample_parameter);
        current_log_likelihoods.push_back(new_sample);
        if (verbose) {
          std::clog << "Initial sample " << i << ": L = " << new_sample.first << std::endl;
        }
        update_min_likelihood(new_sample_likelihood);
      }

      // Setting a global costant
      std::size_t index_of_quantile = (std::size_t)((Real(1.)-_shrink)*Real(n_likelihoods_per_level));
      Real exponential_weight_rateo = exp(Real(1.)/Real(backtracking));
      Real inverse_exponential_weight_rateo = Real(1.) / exponential_weight_rateo;

      // Set last MCMC step
      last_mc_sample = std::make_pair(*current_log_likelihoods.rbegin(), 0);
      last_mc_prior = _model.prior(last_mc_sample.first.second);

      // Level Steps
      for (std::size_t l = 1; l <= _n_levels; l++) {
        // Extracting current level
        assert(current_log_likelihoods.size() == n_likelihoods_per_level);
        std::nth_element(current_log_likelihoods.begin(), current_log_likelihoods.begin()+index_of_quantile, current_log_likelihoods.end());
        typename std::vector<Sample>::iterator current_level_it = (current_log_likelihoods.begin()+index_of_quantile);
        Sample current_level = *current_level_it;

        // Updating structures
        level[l] = current_level;
        current_log_likelihoods.erase(current_log_likelihoods.begin(), current_level_it);
        if (verbose) {
          std::clog << "Level " << l << ", L = " << level[l].first << std::endl;
        }

        // New samples: level±1, pior constrained sample from Model
        std::uniform_int_distribution<int> level_changing_distribution(0, 1);
        std::uniform_real_distribution<Real> unitary_uniform(Real(0.), Real(1.));
        while (current_log_likelihoods.size() < n_likelihoods_per_level) {
          // Generating candidate
          std::size_t candidate_mc_level;
          if (last_mc_sample.second == 0) {
            candidate_mc_level = 1;
          } else if (last_mc_sample.second == l) {
            candidate_mc_level = l-1;
          } else {
            if (level_changing_distribution(_rng) == 0) {
              candidate_mc_level = last_mc_sample.second - 1;
            } else {
              candidate_mc_level = last_mc_sample.second + 1;
            }
          }
          //std::cout << l << ' ' << last_mc_sample.second << ' ' << candidate_mc_level << std::endl;
          Parameter candidate_mc_parameter;
          if (candidate_mc_level == 0) {
            candidate_mc_parameter = _model.prior_sample();
          } else {
            candidate_mc_parameter = _model.constrainted_prior_sample(level[candidate_mc_level]);
          }
          Real candidate_mc_log_likelihood = _model.log_likelihood(candidate_mc_parameter);
          Real candidate_mc_prior = _model.prior(candidate_mc_parameter);

          // Metropolis-Hasting acceptance with exponential weights
          Real acceptance = (candidate_mc_level > last_mc_sample.second ? exponential_weight_rateo : inverse_exponential_weight_rateo) *
                            candidate_mc_prior / last_mc_prior *
                            (candidate_mc_level > last_mc_sample.second ? X_rateo_with_next(last_mc_sample.second) : X_rateo_with_next(candidate_mc_level));
          //std::cout << (candidate_mc_level > last_mc_sample.second ? exponential_weight_rateo : inverse_exponential_weight_rateo) << ' ' <<  ' ' << (candidate_mc_level > last_mc_sample.second ? X_rateo_with_next(last_mc_sample.second) : X_rateo_with_next(candidate_mc_level)) << std::endl;
          if (unitary_uniform(_rng) <= acceptance) {
            last_mc_sample = std::make_pair(std::make_pair(candidate_mc_log_likelihood, candidate_mc_parameter), candidate_mc_level);
            last_mc_prior = candidate_mc_prior;
            //std::cout << "ACC" << std::endl;
          }

          // Updating likelihoods and counters
          if (last_mc_sample.first.first > current_level.first) {
            current_log_likelihoods.push_back(last_mc_sample.first);
          }
          if (last_mc_sample.second < l) { // Only if I already know the likelihood of the level above
            manage_counters_for_X_rateo(last_mc_sample.second, last_mc_sample.first.first);
          }
        }
      }
      
      built_levels_flag = true;
    }
    
    void generate_samples(std::size_t n_samples, bool verbose = false) {
      if (not built_levels_flag) {
        std::cerr << "Fatal error: generate_samples() called before build_levels()" << std::endl;
        exit(1);
      }
      std::uniform_int_distribution<std::size_t> level_uniform_distribution(0, _n_levels);
      for (std::size_t i = 1; i <= n_samples; i++) {
        // Sample
        std::size_t sample_level = level_uniform_distribution(_rng);
        Parameter sample_parameter;
        if (sample_level != 0) {
          sample_parameter = _model.constrainted_prior_sample(level[sample_level]);
        } else { // Level 0 is a special case
          sample_parameter = _model.prior_sample();
        }
        Real sample_likelihood = _model.log_likelihood(sample_parameter);
        Sample sample = std::make_pair(sample_likelihood, sample_parameter);

        // Save samples
        std::size_t sample_true_level = (std::lower_bound(level.begin(), level.end(), sample) - 1) - level.begin(); // true because level are nested; a sample from level j can be also contained in level j+1
        collected_samples_at_level[sample_true_level].push_back(std::make_pair(sample_likelihood, sample_parameter));
        if (verbose) {
          std::clog << "Saved sample " << i << ". Measured Likelihood: " << sample_likelihood << ", lower and upper bound: " << level[sample_true_level].first << ' ' << (sample_true_level != _n_levels ? level[sample_true_level+1].first : -1)  << std::endl;
        }
        //Update counters
        manage_counters_for_X_rateo(sample_level, sample_likelihood);
      }
    }

    Real log_evidence(bool verbose = false) {
      if (not built_levels_flag) {
        std::cerr << "Fatal error: log_evidence() called before build_levels()" << std::endl;
        exit(1);
      }
      Real logZ = Real(-std::numeric_limits<Real>::infinity());
      Real lastX = Real(1.);
      for (std::size_t l = 0; l <= _n_levels; l++) {
        Real nextX = lastX * X_rateo_with_next(l);
        
        Real level_average = (l != 0 ? level[l].first : min_likelihood);
        for (Sample& s: collected_samples_at_level[l]) {
          level_average += s.first;
        }
        level_average /= Real(collected_samples_at_level[l].size()+1);

        Real logBin = level_average + std::log(lastX-nextX);
        logZ = log_sum(logZ, logBin);
        lastX = nextX;

        if (verbose) {
          std::clog << "Level " << l << ": L = " << level[l].first << ", n = " << collected_samples_at_level[l].size() << ", rateo = " << X_rateo_with_next(l) << " c = "  << level_counter_after_next[l] << ", level_average = " << level_average << std::endl;
        }
      }

      return logZ;
    }

    Real likelihood_at_level(std::size_t j) {
      return level[j].first;
    }
};


}

#endif