
#include <iostream>
#include <random>
#include <cmath>
#include <cassert>

#include <Model.cpp>
#include <NestedSampling.cpp>

using namespace std;



class UniformPriorPolynomialLikelihood: public ns::Model<double, double> {
  public:
    using Real = double;
    using Sample = pair<double,double>;

    double a, b, lwl, hgl, power;
    std::default_random_engine generator;
    std::uniform_real_distribution<double> prior_dist;
    std::uniform_real_distribution<double> unitary_uniform;
    UniformPriorPolynomialLikelihood(double power, double a, double b, double lwl, double hgl, unsigned seed = 1) :
      a(a),
      b(b),
      lwl(lwl),
      hgl(hgl),
      power(power),
      generator(seed),
      prior_dist(a,b),
      unitary_uniform(0., 1.)
    {
      assert(a < b);
      assert(lwl < hgl);
    }

    double prior(const double&) {
      return 1./(b-a);
    }

    double log_likelihood(const double& t) {
      if (t < a or t > b) {
        return Real(-std::numeric_limits<double>::infinity());;
      }
      return power*log((t-a)/(b-a)*(hgl-lwl));
    }

    double prior_sample() {
      return prior_dist(generator);
    }

    double constrainted_prior_sample(const Sample& l) {
      double el = exp(l.first/ power);
      assert(el <= hgl);
      return a + (el + unitary_uniform(generator)*(hgl - el))/ (hgl - lwl) * (b-a);
    }
};

int main() {
  const unsigned seed = 126863;
  const double pow = 10.;

  std::mt19937 rng(seed);
  UniformPriorPolynomialLikelihood m(pow, 0., 1., 0., 1., seed);

  ns::NestedSampling<UniformPriorPolynomialLikelihood> ns(m, 30000, 500);

  ns.run(false);

  auto crude = ns.crude_log_evidence();
  cout << "Crude: " << crude.first << "±" << crude.second << endl;
  cout << (crude.first - log(1./(pow+1.)))/crude.second << endl;

  auto mc = ns.montecarlo_log_evidence(100, rng);
  cout << "Montecarlo: "  << mc.first << "±" << mc.second << endl;
  cout << (mc.first - log(1./(pow+1.)))/mc.second << endl;
  ns.save_samples("out.txt");
}