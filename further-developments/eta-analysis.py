import numpy as np
import matplotlib.pyplot as plt
import sys

fs = 15.

plt.rc('text', usetex=True)
plt.rc('text.latex', preamble='\\usepackage{amsmath} \\usepackage{siunitx} \\DeclareMathOperator\\erf{erf}')
plt.style.use('seaborn')

etas = np.linspace(0.01, .99, 99, endpoint=True)
RUN_PER_ETA = 329

def main():
  directory = sys.argv[1]
  logZ = []
  u_logZ = []
  err =  []
  u_err = []

  how_many_missing = {eta:0 for eta in etas}

  for eta in etas:
    logZ_ = []
    err_  = []
    for seq in range(1, RUN_PER_ETA+1):
      try:
        logZ__, err__ = np.loadtxt(f'{directory}/{eta}-{seq}.txt', unpack=True)
        logZ_.append(logZ__)
        err_.append(err__)
      except OSError:
        how_many_missing[eta] += 1
        print(f'File {eta}-{seq}.txt not found - skipping')
      except ValueError:
        how_many_missing[eta] += 1
        print(f'File {eta}-{seq}.txt not formatted correctly - skipping')

    logZ_ = np.array(logZ_)
    err_  = np.array(err_)

    logZ.append(np.mean(logZ_))
    u_logZ.append(np.std(logZ_, ddof=1))

    err.append(np.mean(err_))
    u_err.append(np.std(err_, ddof=1))

  total_missing = 0
  for eta in etas:
    total_missing += how_many_missing[eta]
    print(f'{eta} has {how_many_missing[eta]} missing files')
  print(f'Total file missing {total_missing}')
  

  logZ = np.array(logZ)
  err  = abs(np.array(err))
  u_logZ = np.array(u_logZ)
  u_err  = np.array(u_err)
  
  fig, ax = plt.subplots(figsize=(12,9),sharex=True)
  fig = plt.figure()
  fig.set_size_inches((12,9))
  ax = plt.subplot2grid((12,10), (0,0), colspan = 12, rowspan = 5, fig = fig)
  ax2 = plt.subplot2grid((12,10), (5,0), colspan = 12, rowspan = 5, fig = fig)
  fig.subplots_adjust(hspace=1.)

  ax2.set_xlabel("$\eta$", fontsize= fs)
  ax.set_ylabel("$\\log{Z}$", fontsize= fs)
  ax.tick_params(axis='y', labelsize=fs)
  ax.tick_params(axis='x', labelsize=0.)
  ax2.tick_params(axis='both', labelsize=fs)
  ax.set_xlim(0.,1.)

  ax2.set_yscale('log')
  ax2.set_ylabel('$E_r$', fontsize= fs)

  ax.plot([0., 1.], [-160.948, -160.948], ls='-.')
  ax.errorbar(etas, logZ, u_logZ, ls='',marker='.', ms=11., c='red')
  ax2.errorbar(etas, err, u_err, ls='',marker='.', ms=11., c='red')

  fig.savefig('eta.pdf', format = 'pdf', bbox_inches = 'tight')




if __name__ == "__main__":
  main()