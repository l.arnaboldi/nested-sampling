#include <iostream>
#include <string>
#include <random>
#include <cmath>
#include <array>
#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <iomanip>

#include <DiffusiveNestedSampling.cpp>
#include "model.cpp"




int main(int argc, char *argv[]) {
  assert(argc > 3);
  const double shrink(std::atof(argv[1]));
  const int seed(std::atoi(argv[2]));
  const std::string output_file(argv[3]);

  const unsigned dimension = 50;
  const double   radius = 45.;
  const unsigned levels = unsigned(7000*std::log(0.95)/log(shrink))+1;
  const double   regularization = 500.;
  const double   backtracking = 10.;
  const double   enforce = 15.;
  const unsigned likelihoods_to_build_level = 10000;
  const unsigned max_likelihoods_calls = 30000000;
  const bool verbose = false;

  const double logZ_realvalue = -ns::hypersphere_log_volume(dimension, radius);
  std::mt19937 rng(seed);

  MultiDimensionalNormal<dimension, double, std::mt19937> mn(radius, rng);
  ns::DiffusiveNestedSampling<MultiDimensionalNormal<dimension, double, std::mt19937>, std::mt19937> dns(mn, rng, levels, regularization, enforce, shrink);

  dns.build_levels(likelihoods_to_build_level, backtracking, verbose);
  assert(max_likelihoods_calls > mn.likelihood_calls());
  dns.generate_samples(max_likelihoods_calls - mn.likelihood_calls(), verbose);
  double dns_logZ = dns.log_evidence(verbose);

  std::ofstream fout(output_file);
  fout << std::fixed << std::setprecision(12);
  fout << dns_logZ << ' ' << (dns_logZ - logZ_realvalue)/logZ_realvalue << std::endl;
  if (verbose) {
    std::clog << "logZ true value = " << logZ_realvalue << std::endl;
    std::clog << "Likelihood calls: " << mn.likelihood_calls() << std::endl;
    std::clog << "DNS logZ estimation: " << dns_logZ << std::endl;
    std::clog << "Relative error " << (dns_logZ - logZ_realvalue)/logZ_realvalue << std::endl;
  }
}