from pathlib import Path
import sys
import subprocess
import numpy as np
from multiprocessing.pool import ThreadPool
from pathlib import Path

etas = np.linspace(0.01, .99, 99, endpoint=True)
N_PROCESS = 9
RUN_PER_ETA = 500

def run_simulation(eta_command, eta, seq, directory):
  # Check if the file exists
  the_file = Path(f'{directory}/{eta}-{seq}.txt')
  if the_file.is_file():
    print(f'Skipping eta={eta}, number {seq}, file exists.')
    return

  # Runnning the process
  print(f'Starting eta={eta}, number {seq}')
  subprocess.run([eta_command, str(eta), str(np.random.randint(int(2e9))), f'{directory}/{eta}-{seq}.txt'],
                 stdout=subprocess.DEVNULL,
                 stderr=subprocess.DEVNULL)
  print(f'Computed eta={eta}, number {seq}')

def main():
  directory = sys.argv[1]
  eta_command = sys.argv[2]

  tp = ThreadPool(N_PROCESS)

  for seq in range(1, RUN_PER_ETA+1):
    for eta in etas:
      tp.apply_async(run_simulation, (eta_command, eta, seq, directory))
  tp.close()
  tp.join()

if __name__ == "__main__":
  main()
