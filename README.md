# (Diffusive) Nested Sampling
My implementation and seminar on the algorithms: _Nested Sampling_ and _Diffusive Nested Sampling_.

You can find [here](https://uz.sns.it/~arna/static_/pdf/bayesian-probability/slides-handout.pdf) the slides I've prepared.
