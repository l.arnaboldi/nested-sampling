#include <Utility.cpp>
#include <Model.cpp>

template<unsigned dimension, typename Real, class RandomGenerator>
class MultiDimensionalNormal: public ns::Model<Real, std::array<Real, dimension>>  {
  private :
    Real likelihood_offset;
    Real _prior_radius;
    RandomGenerator& _rng;
    std::normal_distribution<Real> unitary_normal;
    std::uniform_real_distribution<Real> unitary_uniform;
    unsigned likelihood_call_counter = 0;

    Real prior_normalization;
  public: 
    using Parameter = std::array<Real, dimension>;
    using typename ns::Model<Real, std::array<Real, dimension>>::Sample;

    MultiDimensionalNormal(Real prior_radius, RandomGenerator& rng) :
      _prior_radius(prior_radius),
      _rng(rng),
      unitary_normal(Real(0.), Real(1.)),
      unitary_uniform(Real(0.), Real(1.))
    {
      likelihood_offset = - Real(dimension)/Real(2.) * std::log(Real(2.) * ns::pi<Real>);
      prior_normalization = std::exp(-ns::hypersphere_log_volume(dimension, prior_radius));
    }

    Parameter sample_unitary_ball() {
      std::array<Real, dimension> sample;
      for (auto& x: sample) {
        x = unitary_normal(_rng);
      }

      Real factor = ns::uniform2beta_alpha_one(dimension, unitary_uniform(_rng)) / ns::vector_norm(sample);
      for (auto& x: sample) {
        x *= factor;
      }
      return sample;
    }

    Parameter sample_ball(Real radius) {
      Parameter sample = sample_unitary_ball();
      for (auto& x: sample) {
        x *= radius;
      }
      return sample;
    }

    Real prior(const Parameter&) {
      return prior_normalization;
    }

    Real log_likelihood(const Parameter& t) {
      likelihood_call_counter++;
      Real lh = Real(0.);
      for (const auto& x: t) {
        lh -= x*x;
      }
      lh /= Real(2.);
      return lh + likelihood_offset;
    }

    Parameter prior_sample() {
      return sample_ball(_prior_radius);
    }
  
    Parameter constrainted_prior_sample(const Sample& t) {
      Real constrained_radius = ns::vector_norm(t.second);
      return sample_ball(constrained_radius);
    }

    unsigned likelihood_calls() {
      return likelihood_call_counter;
    }
};