import numpy as np
import matplotlib.pyplot as plt
from scipy.special import erf

fs = 20

plt.rc('text', usetex=True)
plt.rc('text.latex', preamble='\\usepackage{amsmath} \\usepackage{siunitx} \\DeclareMathOperator\\erf{erf}')
plt.style.use('seaborn')

fig, ax = plt.subplots(figsize=(7,6))
ax.set_xlabel("$R$", fontsize= fs)
ax.set_ylabel("$1-\\left[\\erf{\\left(\\frac{R}{10}\\right)}\\right]^{50}$", fontsize= fs)

ax.tick_params(axis='both', labelsize=fs )

ls = np.linspace(0., 55., 1000) #, dtype=np.longdouble)
def f(x):
  return 1. - (erf(x/10.))**50

ax.plot(ls, f(ls))
ax.set_yscale('log')
ax.set_xlim(0.,55.)

fig.savefig('setting-radius.pdf', format = 'pdf', bbox_inches = 'tight')