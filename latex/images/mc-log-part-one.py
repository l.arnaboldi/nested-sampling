import numpy as np
import matplotlib.pyplot as plt
from scipy.special import erf

fs = 25

m, logZ, err, sdis, errrel = np.loadtxt('part-one-montecarlo-log.txt', unpack = True)
sdis = abs(sdis)

plt.rc('text', usetex=True)
plt.rc('text.latex', preamble='\\usepackage{amsmath} \\usepackage{siunitx} \\DeclareMathOperator\\erf{erf}')
plt.style.use('seaborn')

fig, ax = plt.subplots(figsize=(12,9),sharex=True)
fig = plt.figure()
fig.set_size_inches((12,9))
ax = plt.subplot2grid((12,9), (0,0), colspan = 9, rowspan = 6, fig = fig)
ax2 = plt.subplot2grid((12,9), (6,0), colspan = 9, rowspan = 3, fig = fig)
fig.subplots_adjust(hspace=0)

ax2.set_xlabel("Montecarlo Samples", fontsize= fs)
ax.set_ylabel("$\\log{Z}$", fontsize= fs)
ax.tick_params(axis='y', labelsize=fs)
ax.tick_params(axis='x', labelsize=0.)
ax2.tick_params(axis='both', labelsize=fs)
ax.set_xlim(1,1e4)
ax.plot([0.1, 11000], [-160.948, -160.948], ls='-.')

ax.errorbar(m, logZ, err, ls='', label = 'MonteCarlo')
ax.errorbar(1.5, -161.01, 0.036, ls='', c='red', label = 'Crude')
ax.set_xscale('log')
ax.legend(fontsize=fs)

ax2.set_yscale('log')
ax2.set_xscale('log')
ax2.plot(m, sdis)
ax2.set_ylabel('$\\sigma$-distance', fontsize= fs)
fig.savefig('part-one-montecarlo-log.pdf', format = 'pdf', bbox_inches = 'tight')