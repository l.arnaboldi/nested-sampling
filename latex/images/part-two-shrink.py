import numpy as np
import matplotlib.pyplot as plt
from scipy.special import erf

fs = 25

sh, logZ, relerr = np.loadtxt('part-two-shrink.txt', unpack = True)

plt.rc('text', usetex=True)
plt.rc('text.latex', preamble='\\usepackage{amsmath} \\usepackage{siunitx} \\DeclareMathOperator\\erf{erf}')
plt.style.use('seaborn')

fig, ax = plt.subplots(figsize=(12,9),sharex=True)
fig = plt.figure()
fig.set_size_inches((12,9))
ax = plt.subplot2grid((12,10), (0,0), colspan = 12, rowspan = 5, fig = fig)
ax2 = plt.subplot2grid((12,10), (5,0), colspan = 12, rowspan = 5, fig = fig)
fig.subplots_adjust(hspace=1.)

ax2.set_xlabel("$\eta$", fontsize= fs)
ax.set_ylabel("$\\log{Z}$", fontsize= fs)
ax.tick_params(axis='y', labelsize=fs)
ax.tick_params(axis='x', labelsize=0.)
ax2.tick_params(axis='both', labelsize=fs)
ax.set_xlim(0.,1.)
ax.plot([0., 1.], [-160.948, -160.948], ls='-.')
ax.plot(sh, logZ, ls='',marker='.', ms=11., c='red')
ax2.plot(sh, relerr)

ax2.set_yscale('log')
# ax2.set_xscale('log')
ax2.set_ylabel('$E_r$', fontsize= fs)
fig.savefig('part-two-shrink.pdf', format = 'pdf', bbox_inches = 'tight')