from pygments.lexers.compiled import CppLexer
from pygments.token import Name, Keyword

class CustomCppLexer(CppLexer):
    name = 'CustromCpp'
    aliases = ['customcpp']

    EXTRA_TYPES = ['Real', 'Parameter', 'Model', 'NestedSampling']


    def get_tokens_unprocessed(self, text):
        for index, token, value in CppLexer.get_tokens_unprocessed(self, text):
            if token is Name and value in self.EXTRA_TYPES:
                yield index, Name.Class, value
            else:
                yield index, token, value