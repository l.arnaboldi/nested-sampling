\documentclass[10pt,a4paper]{article}

\usepackage[american]{babel}
\usepackage[utf8]{inputenc}
\usepackage[paper=a4paper, left=2.5cm, right=2.5cm, top=2.5cm, bottom=2.5cm]{geometry}
\usepackage[section]{placeins}
\usepackage[indent=0cm]{parskip}
\usepackage[colorlinks, linkcolor=blue, urlcolor=magenta, citecolor=teal, bookmarks]{hyperref}
\usepackage{graphicx}
\usepackage{bbold}
\usepackage{hyperref}
\usepackage{verbatim}
\usepackage{tikz}
\usetikzlibrary{calc}
\usepackage[backend=bibtex,
            citestyle=authoryear,
            style=alphabetic]{biblatex}
\addbibresource{bibliography.bib}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{amsmath}
\usepackage{dsfont}

\DeclareMathOperator\erf{erf}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\newcommand{\Prob}[1]{\mathds{P}{\left( #1\right)}}
\newcommand{\ProbPed}[2]{\mathds{P}_{#2}{\left( #1\right)}}
\newcommand{\Prior}[1]{\pi{\left( #1\right)}}
\newcommand{\Post}[1]{p{\left( #1\right)}}
\newcommand{\Likel}[1]{\mathcal{L}{\left(#1\right)}}
\newcommand\tLikel[1]{\tilde{\mathcal{L}} \left(#1\right)}
%\newcommand{\tLikel}[1]{\tilde{\mathcal{L}}{\left(#1\right)}}

\usepackage{bm}
\renewcommand{\vec}[1]{\bm{#1}}


\title{Notes on \emph{Nested Sampling Algorithm}}
\author{Luca Arnaboldi}

\begin{document}
  \maketitle
  \begin{abstract}
    Here are some notes I've written during the preparation of the seminar on Nested Sampling Algorithm that I gave at the exam of the course \emph{Introduction to Bayesian Probability} a.y. 2020-2021.\\
    This document is far from being exhaustive. It has been used just for organizing all the papers I read.
  \end{abstract}
  \tableofcontents
  \newpage
  
  \section{Notation}
  \begin{itemize}
    \item \emph{parameter space}: \(\Theta\) is the domain of the parameter to be estimed. \(\Prior{\theta}\) is the prior, \(\Post{\theta}\) is the posterior.
    \item \emph{likelihood}: \(\Likel{\theta}\). It already includes the measured data.
    \item \emph{evidence}:
    \[Z = \int_\Theta \Likel{\theta} \Prior{\theta} \diff\theta \]
    \item \emph{prior mass}: \(X\colon \Theta \to [0,1]\)
    \[X{(\theta)} = \int_{\left\{\theta'\in\Theta\middle|\Likel{\theta'}>\Likel{\theta}\right\}}
                        \Prior{\theta'} \diff\theta' \]
    \item \emph{likelihood as function of prior mass}:
    \[\tLikel{x} = \Likel{X^{-1}{(x)}}\]
    The function \(X^{-1}{(\cdot)}\) is not always well defined, but it's not always strictly necessary.
    More details in Paragraph \ref{subsec:massinverse}.
    \item \emph{evidence as integral over prior mass}:
    \[ Z = \int_0^1 \tLikel{x}\diff x\]
    
  \end{itemize}

  
  \section{Classic Nested Sampling}
  The two main papers I've used for the preparation of this part of the seminar are \cite{skilling2006nested} and Chap. 9 of \cite{sivia2006data}. Here I'm writing only things I think that need to be explained better.
  
  \subsection{Inverse of the function \(X\)} \label{subsec:massinverse}
  We used the inverse of the prior mass function for changing variable, but this may not exists since there could be different \(\theta\)s with the same likelihood so that the function \(X\) is not injective. This is not a problem if all the points with the same likelihood has also the same image through \(X\):
  \[ \tLikel{\cdot} \text{ is well-defined iff }
      \Big(\forall \theta, \theta':\quad  \Likel{\theta} = \Likel{\theta'} \implies X{(\theta)}=X{(\theta')}\Big).\]
  This condition is equivalent to ask that likelihood contours has null measure under prior
  \footnote{If this happens there is a platoue in \(\tLikel{}\) graph and so nested sampling could have problems in producing the correct output, but it's not the thing we are dealing right now.}:
  \[
  	\int_{\left\{\theta\in\Theta\middle|\Likel{\theta}=\lambda\right\}} \Prior{\theta}\diff\theta = 0
  	 \quad\forall \lambda\in\Likel{\Theta}.
  \]
  Note that if we are using explicitly \(X^{-1}\) to recover some values of the space \(\Theta\) it's necessary to pick uniformly a \(\theta\) from all values that have the corresponding likelihood, it's not sufficient to choose always the same.
  
  \subsection{Measure Theory Hypothesis}
  A nice discussion of measure theory formalism applied to Nested Sampling can be found in the Appendix C of this \cite{feroz2013importance} paper. Other result are exposed in this \cite{chopin2010properties}.
  
  The starting key hypothesis is that both prior and posterior probabilities have their own Radon–Nikodym derivative (\(\pi\) and \(p\) respectively)  respect to the same base measure of the space \(\Theta\).
  Another condition we need is that the map \(X\) is measurable and this traduce to asking \textbf{\(\Likel{\theta}\) continuous almost everywhere}. Moreover, for using the Riemann sum to compute the integral of the likelihood, the measure induced by \(X\) on \([0,1]\) must be absolutely continuous respect to Lebesgue measure. A sufficient condition is that \textbf{\(\Likel{\theta}\) has connect support}, but a weaker hypothesis may work.
  
  \subsection{Estimate the uncertainty on \(\log Z\) using Monte Carlo}
  The procedure is really easy, but at first read from \cite{skilling2006nested} it was not so clear to me, so I prefer clarifying it here. 
  
  The values of \emph{\(\tilde{\mathcal{L}}_i\) are sampled only once} and then used multiple times. What changes in the different estimations of \(\log Z\) are the bins' widths. In the naive implementations of nested sampling  expected  values of bins width are used, but if we want to estimate the error more correctly we can sample shrink factors from their distribution (which is known) and collect a statics on the bins, that can be used for an estimation of errors on \(\log Z\).
  
  \subsubsection{Sampling shrink factors}
  We recall that bins are computed from prior mass samples values \(x_i\):
  \[x_i = \prod_{j=1}^i t_j.\]
  The distribution of \(\vec{t}\) is know to be
  \[\Prob{\vec{t},\vec{t}+\diff\vec{t}} = \prod_{j=1}^M N t_j^{N-1}\diff {t_j} \]
  which is factorizable, so we simply have to sample \(\ProbPed{t,t+\diff t}{t} = N t^{N-1}\diff {t}\).
  
  Skilling suggest to use MC sampling, but it seems overkill to me because we can use a change of variable to induce the desidered density staring from an uniform distribution.
  Let \(U\) an uniform random variable over \([0,1]\). Let impose this condition:
  \begin{align}
 	 \ProbPed{[0,t]}{t} &= \ProbPed{[0,u]}{U} \nonumber\\
 	 \int_0^t N t'^{N-1} \diff t' &= \int_0^u  \diff u' \nonumber\\
 	 t^N &= u \nonumber\\
 	 t &= \sqrt[N]{u} \label{eq:samplemax}
  \end{align}
  So using relation \eqref{eq:samplemax} we can use a sample from an uniform to get a sample from the desired distribution. 
  
  
  \section{Diffusive Nested Sampling}
  The only article in literature that describes the nested sampling algorithm is \cite{brewer2011diffusive}. Here is a list of things that are not clare in my opinion:
  \begin{itemize}
    \item It says that every likelihood is saved, but (as one may expect) there is a decorrelation time of the Markov Chain. In the particular example of the paper, we save the particle likelihood only once in 10'000 MCMC steps.
    \item The \emph{only} reason why we keep sampling when we have already built all the levels is that more samples give us a better estimation of the \(X\)s.
    \item It's never explicitly said how to estimate the evidence. What I've understood is that the algorithm is returning couples \((\theta, \Likel{\theta})\) as samples from the posterior. One can now estimate the evidence by assigning the \(X\) value of the sample using level likelihoods. When every sample has a corresponding \(X\) the evidence is just a Riemann sum. My personal consideration not pointed out in the paper:
    \begin{itemize}
      \item Probably the algorithm will produce more than a sample per interval \([X_j, X_{j+1}]\). I think that the correct thing to do is average over them.
      \item Trapezoidal integration is not useful in this case. Maybe a different approach for estimating the evidence could be to use only the level delimiters likelihoods, where trapezoidal can be used. This seems to me a waste of computational energy, since having the likelihoods does not cost anything more, and once I have them it's unproductive not using them.
    \end{itemize}
  \end{itemize}

  \subsection{The motivation}
  What may not be clear at first approach with \emph{diffusive nested sampling} is why should I sample a mixture instead of the constrained likelihood. 
  
  The answer can be better understood looking at multimodal likelihood function with a sharp peak. In Classic Nested Sampling we generate the new alive particle by starting a MCMC from those ones we already have. It may happen that no initial sample belong to the sharp peak and the hard likelihood constraints disconnect it from the other, making it undetectable. Diffusive Nested Sampling tries to avoid this problem by using mixtures to make regions never disconnected, since there is always a non-zero probability of transition between peaks. A picture of a possible NS run that misses a peak it's represented in Figure~\ref{fig:hiddenpeak}.
  \begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{images/hiddenpeak}
    \caption{Representation of a NS run with 3 particles and 5 level where a likelihood peak is never discovered. Since none of the initial points sampled from the prior was near the rightmost peak, 
    new particles created can't discover it beacuse of likelihood constraint in the MCMC algorithm. The evidence computed in this case is incorrect.}
    \label{fig:hiddenpeak}
  \end{figure}
  
  Another way to see why Diffusive Nested Sampling is working is by noting that the mixture is essentially approximating the posterior using overlapping different layers of prior. A graphical representation of this intuition can be found in Figure~\ref{fig:mixtureprior}.
  \begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{images/priormixture}
    \caption{Diffusive Nested Sampling replaces the target posterior distribution by a mixture of constrained priors. The density is non-negligible everywhere (since the prior is a component of this distribution), and states intermediate between the two phases in the top-right mode are appropriately up-weighted. Source~\cite{brewer2014inference}.}
    \label{fig:mixtureprior}
  \end{figure}

  \newpage
  \section{Exam Problem}
  The prior used is uniform over the D dimensional hypersphere of radius \(R\), centered in the origin:
  \[
    \Prior{\vec{\theta}} = \frac{1}{V_D{(R)}} \chi_{[0,R]}{\left(\left\lVert \vec{\theta}\right\rVert\right)}
  \]
  where \(V_D{(R)}\) is the volume of the \(D\)-sphere
  \[
    V_D{(R)} = \frac{\pi^{\frac{D}{2}}}{\Gamma{\left(\frac{D}2 + 1\right)}} R^D.
  \]
  The likelihood function used in our computation is a \(D\) dimensional Gaussian with covariance matrix equal to identity:
  \[
    \Likel{\vec{\theta}} = \prod_{i=1}^{D} \frac{1}{\sqrt{2\pi}} \exp{\left[-\frac{\theta_i^2}{2} \right]}.
  \]
  The log-likelihood is:
  \[
  \log\Likel{\vec{\theta}} = -\frac{D}{2}\log(2\pi)-\sum_{i=1}^{D} \frac{\theta_i^2}{2}.
  \]
  
  The nested sampling algorithm estimates the integral
  \begin{align*}
    Z{(R, D)} &= \int_{\mathds{R}^D}\Prior{\vec{\theta}}\Likel{\vec{\theta}}\diff^D\vec{\theta} \\
      &= \int_{\mathds{R}^D} \frac{1}{V_D{(R)}} \chi_{[0,R]}{\left(\left\lVert \vec{\theta}\right\rVert\right)}
                             \prod_{i=1}^{D} \frac{1}{\sqrt{2\pi}} \exp{\left[-\frac{\theta_i^2}{2} \right]}\diff^D\vec{\theta}\\
      &= \frac{1}{V_D{(R)}} \int_{\mathcal{B}^D{(R)}} \prod_{i=1}^{D} \frac{1}{\sqrt{2\pi}} \exp{\left[-\frac{\theta_i^2}{2} \right]}\diff^D\vec{\theta}\\
  \end{align*}

  \subsection{Analytical bounds}
  The last integral can be bounded analytically using simple inequalities.
  
  An upper bound can be obtained considering the limit \(R\to +\infty\). In that case the integral can be factorized and we get an easy bound:
  \begin{align*}
    Z{(R, D)} V_D{(R)} &= \int_{\mathcal{B}^D{(R)}} \prod_{i=1}^{D} \frac{1}{\sqrt{2\pi}} \exp{\left[-\frac{\theta_i^2}{2} \right]}\diff\vec{\theta}
    < \int_{\mathcal{B}^D{(+\infty)}} \prod_{i=1}^{D} \frac{1}{\sqrt{2\pi}} \exp{\left[-\frac{\theta_i^2}{2} \right]}\diff\vec{\theta} \\
    &= \int_{\mathds{R}^D} \prod_{i=1}^{D} \frac{1}{\sqrt{2\pi}} \exp{\left[-\frac{\theta_i^2}{2} \right]}\diff\vec{\theta} 
    = \prod_{i=1}^{D} \int_{-\infty}^{+\infty} \frac{1}{\sqrt{2\pi}} \exp{\left[-\frac{\theta_i^2}{2} \right]}\diff{\theta_i}
    = 1^D = 1
  \end{align*}

  The lower bound instead it's not that trivial. It's possible to factorize the domain by considering a sub-region of \(\mathcal{B}^D{(R)}\). Consider the hypercube inscribed in the ball: its side length is \(\ell{(R,D)} = \frac{2R}{\sqrt{D}}\). The hypercube covers the domain
  \[
    \mathcal{HC}{(R,D)} = \left[-\frac{\ell{(R,D)}}2,\frac{\ell{(R,D)}}2\right]^D =
                          \left[-\frac{R}{\sqrt{D}},\frac{R}{\sqrt{D}}\right]^D \subset \mathcal{B}^D{(R)}.
  \]
  The integral over the hypercube it's factorizable and, since the integrand is positive, it give us the bound
   \begin{align*}
    Z{(R, D)} V_D{(R)} &= \int_{\mathcal{B}^D{(R)}} \prod_{i=1}^{D} \frac{1}{\sqrt{2\pi}} \exp{\left[-\frac{\theta_i^2}{2} \right]}\diff\vec{\theta}
    > \int_{\mathcal{HC}{(R,D)}} \prod_{i=1}^{D} \frac{1}{\sqrt{2\pi}} \exp{\left[-\frac{\theta_i^2}{2} \right]}\diff\vec{\theta} \\
    &= \prod_{i=1}^{D} \int_{-\frac{R}{\sqrt{D}}}^{+\frac{R}{\sqrt{D}}} \frac{1}{\sqrt{2\pi}} \exp{\left[-\frac{\theta_i^2}{2} \right]}\diff{\theta_i}
    = \left[\erf{\left(\frac{R}{\sqrt{2D}}\right)}\right]^D 
  \end{align*}

  \subsection{Constrained samples}
  The problem is so simple that we know explicitly the prior mass function and its inverse.
  Nested Sampling could be run using this knowledge and it will perform really well compared to a standard run.
  The point is that  nested sampling algorithm is not necessary at all, since we can numerically integrate the function \(\tLikel{X}\), 
  and for sure NS it is not the best choice to do it.
  
  We can still run the nested sampling assuming to not know the function \(\tLikel{X}\), but taking advantage of the problem symmetries. In fact the constrained samples are again a sample from a \(D\)-dimensional hyperball, that can be sampled easily as exposed in Section~\ref{subsec:samplinghyperball}.
  Therefore is not necessary to use complex techniques such as MCMC to get new particles. This drop the amount of computation time need to run the algorithm. Moreover, the possibility to have independent samples from constrained prior makes the results more accurate since there is no correlation in particles.
  
  In the end we can also run the algorithm using different approaches to sample the prior. Probably the easiest to follow is MCMC, and I'll try it, \textbf{maybe}.
  
  The problem with MCMC is that the \(\delta\) of an exploration has to be finely tuned depending on the constraint applied otherwise the acceptance ration will drop after few steps of the exploration or the sample would be too correlated at the beginning. This tuning ha to be done using the knowledge and the prior of the model. Once realized that we \emph{must} use this information, it's stupid to not take independent sample directly.

  \subsection{Sampling a ball uniformly} \label{subsec:samplinghyperball}
  WLOG we sample the unitary \(D\)-dimensional hyperball. We can sample separately the radius and the direction.
  
  The pdf of the radius is
  \[
    \ProbPed{r,r+\diff r}{r} = D r^{D-1}.
  \]
  
  Sampling a point on the surface of an \(D\)-hypersphere can be do by sampling \(X_i \sim \mathcal{N}(0,1) \quad \forall i = 1, \dots, D\), and then considering the vector
  \[
  \vec{d} = \frac{1}{\sqrt{\sum_{i=1}^{D}X_i}}(X_1,\dots,X_D)
  \]
  
  
  
  \section{Implementation}
  \subsection{Base class \texttt{Model}}
  It's a pure virtual class that defines the methods need for using NestedSampling algorithms.
  
  Both NS and DNS need a \texttt{Model} class with these methods implemented:
  \begin{itemize}
    \item \texttt{prior}
    \item \texttt{sample\_prior}
    \item \texttt{log\_likelihood}
    \item \texttt{constrained\_sample\_prior}
  \end{itemize}
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  \printbibliography
\end{document}