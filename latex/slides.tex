\documentclass{beamer}
\usepackage[american]{babel}
\usepackage[utf8]{inputenc}

\usepackage{amsmath}
\usepackage{dsfont}
\usepackage{mathtools}
\usepackage[linesnumbered,lined,ruled,vlined,commentsnumbered]{algorithm2e}

\usepackage[backend=bibtex,
citestyle=authoryear,
style=alphabetic]{biblatex}
\addbibresource{bibliography.bib}

\usepackage{xcolor}

\usepackage{pgfpages}
\setbeamertemplate{note page}[plain]
%\setbeameroption{show notes on second screen=bottom}
%\pdfinfo{/Keywords (SP-Bottom)}

\usetheme{Madrid}
\usecolortheme{seahorse}
\setbeamercovered{transparent}

\usepackage{minted}
\usemintedstyle{github}

\usepackage{bm}
\renewcommand{\vec}[1]{\bm{#1}}

\usepackage[separate-uncertainty = true]{siunitx}

\DeclareMathOperator\erf{erf}
\DeclareMathOperator\Var{Var}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\newcommand{\Prob}[1]{\mathds{P}{\left( #1\right)}}
\newcommand{\ProbPed}[2]{\mathds{P}_{#2}{\left( #1\right)}}
\newcommand{\Prior}[1]{\pi{\left( #1\right)}}
\newcommand{\Post}[1]{p{\left( #1\right)}}
\newcommand{\Likel}[1]{\mathcal{L}{\left(#1\right)}}
\newcommand\tLikel[1]{\tilde{\mathcal{L}} \left(#1\right)}
\newcommand{\ExpVal}[1]{\mathds{E}{\left[#1\right]}}

\newcommand{\cpp}[1]{\mintinline{CustomCppLexer.py:CustomCppLexer -x}{#1}}
\newcommand{\CPP}[1]{\begin{minted}{CustomCppLexer.py:CustomCppLexer -x}#1\end{minted}}


%% Title page
\title[(Diffusive) Nested Sampling]{(Diffusive) Nested Sampling}
\subtitle{an object-oriented \texttt{C++} implementation}
\author[Luca Arnaboldi]{Luca Arnaboldi}      
\institute[]{Course ``Introduction to Bayesian Probability Theory''}
\date[2021-07-12]{July 12, 2021}

\begin{document}
  \frame[plain,noframenumbering]{\titlepage}
  
  \section{Nested Sampling description}
  \begin{frame}
    \frametitle{Nested Sampling: the setting}
    \begin{columns}
      \column{0.35\textwidth} \centering
        A \emph{model} is defined by:
        \begin{itemize}
          \item \structure{parameters space}: \(\Theta\);
          \item \structure{prior density}: \(\Prior{\theta}\);
          \item \structure{likelihood}: \(\Likel{\theta}\).
        \end{itemize}
      \column{0.10\textwidth} \pause
        \(\xrightarrow[\text{calculation}]{\text{Bayesian}}\)
      \column{0.52\textwidth}
        \begin{itemize}
          \item \structure{evidence}: \(Z \coloneqq \int_\Theta \Likel{\theta} \Prior{\theta} \diff\theta \)
          \item \structure{posterior density}: \(\Post{\theta} \coloneqq \frac{\Likel{\theta}\Prior{\theta}}{Z}\)
        \end{itemize}
    \end{columns}
    \pause
    \begin{exampleblock}{Idea!}
      Change of variable through \emph{cumulative distribution function} to make the problem one dimensional \(X\colon \Theta \to [0,1]\)
      \[X{(\theta)} \coloneqq \int_{\left\{\theta'\in\Theta\middle|\Likel{\theta'}>\Likel{\theta}\right\}}
      \Prior{\theta'} \diff\theta' \]
    \end{exampleblock}
    \pause
    Using \(\diff X = \Prior{\theta}\diff\theta\), evidence can be computed by:
    \[\tLikel{x} \coloneqq \Likel{X^{-1}{(x)}} \quad \implies \quad Z = \int_0^1 \tLikel{x}\diff x\]
    
    %%%%%%%%
    \note{test normal}
    \note[item]{test item}
  \end{frame}

  \begin{frame}
    \frametitle{Why cumulative function with \(\mathcal{L}\)-ordering?}
    \begin{itemize}
      \item \textbf{Dimension reduction}
      \item \(\tLikel{x}\) it's non increasing function;
      \item Prior constrained samples are uniformly distributed in \(X\):
      \[\Prior{\theta|\Likel{\theta}>\mathcal{L}^*} \quad\longleftrightarrow\quad X^{-1}{(u)} \text{ with } u \sim U\left[0,\tilde{\mathcal{L}}^{-1}(\mathcal{L}^*)\right]\]
    \end{itemize}
    \pause
    \begin{alertblock}{Is it always weel defined this construction?}
      No, \emph{but actually yes}.
      \begin{itemize}
        \item We don't need \(X^{-1}\) explicitly, \(\tilde{\mathcal{L}}\) is sufficient. This happens when prior contours has null measure.
        \item Mostre general sufficient hypothesis \cite{feroz2013importance,chopin2010properties}:
          \begin{itemize}
            \item \(p\) and \(\pi\) are Radon-Nikodyn derivative respect to the same measure~of~\(\Theta\);
            \item \(\mathcal{L}\) is continuous almost everywhere;
            \item \(\mathcal{L}\) has connected support.
          \end{itemize}
      \end{itemize} 
    \end{alertblock}
  \end{frame}

  \begin{frame}
    \frametitle{Nested Sampling: the algorithm}
      \begin{columns}
        \column[T]{0.7\textwidth}
         \begin{enumerate}
          \item<1-> Set \(Z = 0\)
          \item<2-> Set of \(N\) \emph{particles}:\(\left\{(\theta, \Likel{\theta})_n\right\}_{n=1,\dots,N}\).
          \item<3-> At step \(j\) in  \(1,\dots, L\):
            \begin{enumerate}
              \item Let $(\theta^*,\mathcal{L}^*)$ lowest-likelihood particle, and remove it from the set;
              \item Estimate \(X^{-1}{(\theta^*)}\) statistically using
                \[X_j = t X_{j-1} \qquad \text{with} \quad \ProbPed{t,t+\diff t}{t} = N t^{N-1}\diff {t}\]
              \item Increase \(Z\) by \(\mathcal{L}^* (X_j-X_{j-1})\);
              \item Sample a new particle with constrained prior \(\Prior{\theta\middle|\Likel{\theta}>\mathcal{L}^*}\)
            \end{enumerate}
          \item<4-> \(Z\) is the computed evidence;
        \end{enumerate}
        \column[T]{0.3\textwidth}
          \includegraphics[width=0.95\textwidth]{images/ns-thetaspace} 
          \includegraphics[width=0.95\textwidth]{images/ns-Xspace}
          {\centerline{\small Credits \cite{feroz2013importance}}}
      \end{columns}
  \end{frame}

  \begin{frame}
    \frametitle{Estimating the uncertainty}
    Two approaches \cite{skilling2006nested} for computing evidence and uncertainty:
    \begin{itemize}
      \item \structure{Crude}: using the \emph{information}: \(\mathcal{H} = \int^1_0 \frac{\tLikel{x}}{Z} \log \frac{\tLikel{x}}{Z} \diff x\) \cite{sivia2006data}
      \[\log Z = \left(\log\left\{\sum_{j=1}^L \mathcal{L}_j \cdot \Big[\exp\ExpVal{\log X_j} - \exp\ExpVal{\log X_{j-1}}\Big]\right\} \pm \sqrt{\frac{\mathcal{H}}{N}}\right)\]
      \pause
      \item \structure{MonteCarlo}:
      Collect a richer statics on \(\log Z\), with \(n_{MC}\) independent samples:
      \begin{enumerate}
        \item Sample random variable \(t\), \(n_{MC} \cdot L \) times;
        \item Build \(n_{MC}\) independent vectors \(\{X_j\}_{j=1,\dots,L}^{(s)}\,\,{s=1,\dots,n_{MC}}\) 
        \item Compute the set \(\left\{\log Z^{(s)}\right\}\), using the same \(\mathcal{L}_j\)
        \item Compute mean and STD as usual MonteCarlo
      \end{enumerate}
      \pause
      \begin{alertblock}{Attention!}
        This procedure \textbf{does not converge} to the true \(\log Z\)~value. \\
        \guillemotleft sample \(\vec{t}\) dozens of times \guillemotright \cite{sivia2006data} seems to me a way to hide the problem.
        If \(n_{MC}\) is large it leads to incorrect error estimation.
      \end{alertblock}
    \end{itemize}
    
  \end{frame}


  \section{Code description of Nested Sampling}
  \begin{frame}
    \frametitle{Implementation}
    Library written in \texttt{C++}. Two classes:
    \begin{itemize}
      \item \cpp{Model}: pure virtual class to be used as father class of implemented models.
      \item \cpp{NestedSampling}: general class that run the NS algorithm on the given model.  
    \end{itemize}
    \pause
    Main Features:
    \begin{itemize}
      \item All the implementation relies only on the \structure{\texttt{C++} standard library}: no external dependencies.
      \item \structure{Header-only}: It does not need to be pre-compiled.
      \item Fully \structure{template}d.
    \end{itemize}
    \pause
    Complete source-code can be download from this repository: \url{https://gitlab.com/l.arnaboldi/nested-sampling}.
  \end{frame}
    
  \begin{frame}
    \frametitle{Class description: \cpp{Model}}
    Member types:
    \begin{itemize}
      \item \cpp{Real}: type used for handling real numbers;
      \item \cpp{Parameter}: type of objects in the space \(\Theta\);
      \item \cpp{Sample}: alias of \cpp{std::pair<Real, Parameter>}, used to store pairs \((\theta, \Likel{\theta})\).
    \end{itemize}
    Pure virtual member functions:
    \pause
    \begin{itemize}
      \item \cpp{Real prior(const Parameter&)}: implementation of \(\pi(\cdot)\);
      \item \cpp{Real log_likelihood(const Parameter&)}: implementation of \(\log{\left[\mathcal{L}(\cdot)\right]}\);
      \item \cpp{Parameter prior_sample()}: return an \emph{indipendent} sample of \(\Prior{\theta}\);
            \note{Say that if a MCMC is used we must set the decorrelation time properly to have independent samples.}
      \item \cpp{Parameter constrainted_prior_sample(const Sample&)}: return an \emph{indipendent} sample of \(\Prior{\theta\middle|\Likel{\theta}>\mathcal{L}^*}\);
    \end{itemize}
    
  \end{frame}

  \begin{frame}
    \frametitle{Class description: \cpp{NestedSampling}}
    Constructor has 3 parameters:
    \begin{enumerate}
      \item \cpp{Model& model}: implentation of the model. Here \cpp{Model} is template argument referring to the model class-name. 
      \item \cpp{const std::size_t n_levels}, \(L\): number of bins used in the integration.
      \item \cpp{const std::size_t n_particles}, \(N\): dimesnion of the set of particles.
    \end{enumerate}
    \pause
    \vfill
    The method that builds the levels is \cpp{void run()}. Implements the algorithm described above.
    \begin{itemize}
      \item use a \cpp{std::priority_queue} to get the minimum efficiently.
      \item stores the \cpp{Sample}s in the data member \cpp{std::vector<Sample> increasing_samples}
    \end{itemize} 
  \end{frame}
  
  \begin{frame}[fragile]
    The two member functions for estimating the uncertainty:
    \begin{itemize}
      \item \cpp{std::pair<Real, Real> crude_log_evidence();}
      \item
        \begin{minted}{CustomCppLexer.py:CustomCppLexer -x}
template <class RandomGenerator>
std::pair<Real, Real> montecarlo_log_evidence(
  std::size_t mc_samples,
  RandomGenerator& rng
);
        \end{minted}
    \end{itemize}
    \pause
    \vspace{10pt}
    The class can be inherited for implementing generic observables over the posterior.
  \end{frame}


  \section{Diffusive Nested Sampling}
  \begin{frame}
    \frametitle{Diffusive Nested Sampling: the algorithm}
    \structure{Parameters}
    \begin{itemize}
      \item \(N\): number of samples needed to build a level.
      \item \(L\): number of levels.
      \item \(\eta\): shrinkage factor (equal to \(e^{-1}\) in the original paper \cite{brewer2011diffusive})
      \item \(\Lambda\): backtracking degree.
    \end{itemize}
    \pause
    \structure{Part 1: build levels}
    \begin{enumerate}
      \item Set \(\mathcal{L}_0 = 0\)
      \item For each \(j\) in \(1,\dots,L\):
        \begin{enumerate}
          \item Sample the mixture (\underline{not necessarily independently})
                \[\sum_{k=0}^{j-1} w_k \Prior{\theta|\Likel{\theta}>\mathcal{L}_k}\quad\text{with } w_k\propto\exp{\left[\frac{k-j}\Lambda\right]}  \]
                until we have \(N\) sample with \(\mathcal{L}>\mathcal{L}_{j-1}\)
          \item We choose as \(\mathcal{L}_j\) the \((1-\eta)\)-quantile of the collected statics.
          \item Remove all values \(<\mathcal{L}_j\) from the set of likelihoods; keep the others for the next step.
        \end{enumerate}
    \end{enumerate}
  \end{frame}

  \begin{frame}
    \begin{exampleblock}{Why fixed shrinkage?}
      We sample the mixture with Metropolis-Hastings algorithm: we need the normalization factors of the \(\Prior{\theta|\Likel{\theta}>\mathcal{L}_k}\) for computing the acceptance.
      This factors are simply \(X_k = \tilde{\mathcal{L}}^{-1}{(\mathcal{L}_k)}\), so the rateo between two consecutive levels \emph{is expected} to be \(\eta\).
    \end{exampleblock}
    \pause
    \structure{Part 2: collecting samples.}
      \begin{itemize}
        \item Change \(w_k\) to uniform and sample the mixture \underline{indipendently} until enough samples are obtained.
        \item Every sample is stored in the corresponding level, based on the likelihood.
      \end{itemize}
    \pause
    \structure{Part 3: estimating the evidence}
      \begin{itemize}
        \item Average the likelihood inside every level.
        \item Evidence is the Riemann sum using levels as bins.
      \end{itemize}
  \end{frame}

  \begin{frame}
    \frametitle{Diffusive Nested Sampling: the motivation}
    \begin{columns}
      \column{0.50\textwidth}
        \includegraphics[width=\textwidth]{images/hiddenpeak}
      \column{0.50\textwidth}
        \structure{Multimodal Likelihoods}.\\
        The main motivation that aimed the developing of~DNS is handling sharply peaked multimodal likelihoods.\\
        Hard constraints clash with MCMC algorithms in finding hidden peaks.
    \end{columns}
    \pause
    \vspace{10pt}
    \begin{columns}
      \column{0.35\textwidth}
        \structure{Posterior is approximated by prior}\\
        The mixture is a superposition of prior in different regions that tends towards the posterior.
      \column{0.65\textwidth}
        \includegraphics[width=\textwidth]{images/priormixture}
        \vspace{-1cm}
        \begin{center}
          {\small Image credits \cite{brewer2014inference}.}
        \end{center}
    \end{columns}
  \end{frame}

  \begin{frame}
    \frametitle{Diffusive Nested Sampling: Enchantments}
    Original paper \cite{brewer2011diffusive} suggest two corrections:
    \begin{itemize}
      \item \structure{Revising the \(X\)s}: Since \(N\) is finite, the rateo between \(X\) of consecutive levels won't be exactly \(\eta\). A better estimation is:
        \[ \frac{X_{j+1}}{X_j} = \frac{n{(\mathcal{L} > \mathcal{L}_{j+1}^*|j)}+C\eta}{n{(j)}+C}\]
        with \(C\) a regularization constant that avoids initial fluctuations.
      \pause
      \item \structure{Enforcing target weighting}:
        \[\alpha^\text{enforce}_{j,k} = \left(\frac{(n_j+C)/(\langle n_j\rangle + C)}{(n_k+C)/(\langle n_k\rangle + C)}\right)^\beta \quad \text{with } \beta \text{ the enforce factor.}\]
        It's argued if this term breaks Markovianity of the algorithm~\cite{robert2009diffusive}.\\
        I've not implemented this because it was not necessary; the problem was avoided by properly setting \(N\) and \(\eta\).
    \end{itemize}
  \end{frame}


  \begin{frame}
    \frametitle{DNS Implementation}
    \begin{columns}
      \column{0.60\textwidth}
         Included in Nested~Sampling library:
         \begin{itemize}
           \item rely on the class \cpp{Model}
           \item fully templated
         \end{itemize}
      \column{0.40\textwidth}
        \begin{alertblock}{Caveat!}
          The model must implement (in)dependence of samples autonomously.
        \end{alertblock}
    \end{columns}
    \vfill
    Three methods:
    \begin{itemize}
      \item \cpp{void build_levels(...)}: build the levels as described in \structure{Part 1}. It implements a Metropolis-Hastings algorithm.\\
            Use the function \cpp{std::nth_element} for computing the quantile in linear time.
      \item \cpp{void generate_samples(...)}: sample the uniform mixture to extract posterior samples.\\
                                              For efficiently assign level to sample it use \cpp{std::lower_bound}. 
      \item \cpp{Real log_evidence()}.
    \end{itemize}
  \end{frame}


  \section{Test Problem}
  \begin{frame}
    \frametitle{Test problem: integrating multivariate normal}
    We are going to compute the evidence on the model
    \[
    \Theta \equiv \mathds{R}^D,
    \qquad
    \Prior{\vec{\theta}} = \frac{1}{V_D{(R)}} \chi_{[0,R]}{\left(\left\lVert \vec{\theta}\right\rVert\right)},
    \qquad
    \Likel{\vec{\theta}} = \prod_{i=1}^{D} \frac{1}{\sqrt{2\pi}} \exp{\left[-\frac{\theta_i^2}{2} \right]},
    \]
    where \(V_D{(R)}\) is the volume of the \(D\)-sphere:
    \(
    V_D{(R)} = \frac{\pi^{\frac{D}{2}}}{\Gamma{\left(\frac{D}2 + 1\right)}} R^D
    \).
    \vfill
    \pause
    The evidence is given by
    \[
    Z{(R, D)}= \frac{1}{V_D{(R)}} \int_{\mathcal{B}^D{(R)}} \prod_{i=1}^{D} \frac{1}{\sqrt{2\pi}} \exp{\left[-\frac{\theta_i^2}{2} \right]}\diff^D\vec{\theta}
    \]
    where \(\mathcal{B}^D{(R)}\) is the ball of radius \(R\) centered in the origin.
  \end{frame}

  \begin{frame}[fragile]
    \frametitle{Implementation: \cpp{MultiDimensionalNormal}}
    \structure{Declaration}
    \begin{minted}{CustomCppLexer.py:CustomCppLexer -x}
template<unsigned dimension, typename Real, class RandGen>
class MultiDimensionalNormal:
  public ns::Model<Real, std::array<Real, dimension>> {...};
    \end{minted}
    \vspace{0.3cm}
    \pause
    \structure{Constructor}
    \begin{minted}{CustomCppLexer.py:CustomCppLexer -x}
MultiDimensionalNormal(Real prior_radius, RandGen& rng);
    \end{minted}
    \vspace{0.3cm}
    \pause
    \structure{Sample a ball uniformly}
    \begin{minted}{CustomCppLexer.py:CustomCppLexer -x}
Parameter sample_ball(Real radius);
    \end{minted}
    \begin{columns}
      \column{0.7\textwidth}
        Sampling radius and direction separately:
        \begin{itemize}
          \item \(\ProbPed{r,r+\diff r}{r} = D r^{D-1}\) (with \cpp{radius = 1})
          \item Point on the \(D\)-hypersphere \cite{hicks1959efficient}
            \[\vec{d} = \frac{1}{\sqrt{\sum_{i=1}^{D}X_i}}(X_1,\dots,X_D) \quad X_i \sim \mathcal{N}(0,1)\]
        \end{itemize}
      \column{0.3\textwidth}
      \begin{alertblock}{What about accpect-reject?}
        Too inefficient in high dimension!\\
        The acceptance at \(D=50\) is \(10^{-28}\).
      \end{alertblock}
    \end{columns}
  \end{frame}

  \begin{frame}
    \frametitle{Implementation: \cpp{MultiDimensionalNormal}}
    The implementation of 4 required methods is now trivial:
    \begin{itemize}
      \item \structure{Pior}: \(\frac{1}{V_D{(R)}}\)
      \item \structure{Log-Likelihood}: \(\log\Likel{\vec{\theta}} = -\frac{D}{2}\log(2\pi)-\sum_{i=1}^{D} \frac{\theta_i^2}{2}\)
      \item \structure{Sample Prior}: sample the ball of radius \(R\)
      \item \structure{Constrained Sample Prior}: sample the ball of radius \(\left\lVert \vec{\theta}\right\rVert\)
        \begin{exampleblock}{Observation!}
          The likelihood is a decreasing function of the norm of the parameter \(\vec{\theta}\).\\
          All the points with likelihood greater than a sample \((\Likel{\vec{\theta}}, \vec{\theta})\) are contained in the ball \(\mathcal{B}^D{(\left\lVert \vec{\theta}\right\rVert)}\).
        \end{exampleblock}
    \end{itemize}
    \vspace{0.5cm}
    \structure{Extra}: there is counter of the calls of the function \cpp{log_likelihood};\\ it is used to compare different algorithms.
  \end{frame}
  

  \begin{frame}
    \frametitle{Choosing \(R\) value}
    We can derive some bounds on the evidence value:
    \[
      \left[\erf{\left(\frac{R}{\sqrt{2D}}\right)}\right]^D < Z{(R, D)} V_D{(R)} < 1
    \]
    \begin{columns}
      \column{0.65\textwidth}
        \includegraphics[width=\textwidth]{images/setting-radius.pdf}
      \column{0.35\textwidth}
        We use \(D = 50\) and set the radius at \(R=45\) so that the bounds are tighter than \(10^{-8}\).
        \[\log{Z} \approx -160.948\]
    \end{columns}
  \end{frame}
  
  \begin{frame}
    \frametitle{Algorithms run}
    \(N\) \(L,\) and \(\eta\) have been set in order to make the \(X\) values the same in all runs.
    The number of likelihood calls have been limited to \(\num{30 000 000}\).
    \vspace{0.5cm}
    \pause
    \begin{columns}[T]
      \column{0.45\textwidth}
        \centerline{\structure{NS}}
        Alive Particles: \(N = \num{80000}\)\\
        Levels: \(L = \num{29920000}\)\\
        \vspace{0.6cm}
        \textbf{Crude}: \(\num{-161.010\pm0.033}\)
        \(\sigma\)-distance: \(1.86\)
        \(E_r = \num{3.89e-4}\)
      
      \pause
      \column{0.55\textwidth}
        \centerline{\structure{DNS}}
          Needed likelihoods: \(N = \num{10 000}\)\\
          Backtracking: \(\Lambda = 10\)\\
          Regularization: \(C = 500\)\\
          \vspace{0.2cm}
          \textbf{Best}: \(\eta = 0.5\)\\
           \(\num{-160.945}\)\\
          \(E_r = \num{1.56e-05}\)
          
    \end{columns}
  \end{frame}

  \begin{frame}
    \begin{columns}
      \frametitle{MonteCarlo Uncertainty in NS}
      \column[T]{0.95\textwidth}
        \includegraphics[width=\textwidth]{images/part-one-montecarlo-log}
      \column[T]{0.05\textwidth}
    \end{columns}
    The estimation is converging, but to the wrong value. The uncertainty of the \(\mathcal{L}_j\)s 
  \end{frame}

  \begin{frame}
    \begin{columns}
      \frametitle{Shrink factor in DNS}
      \column[T]{0.95\textwidth}
      \includegraphics[width=\textwidth]{images/part-two-shrink}
      \column[T]{0.05\textwidth}
    \end{columns}
    It seems that central values perform better, probably due to low \(N\) at extremes.
  \end{frame}

  \section{Conclusions}
  \begin{frame}
    \frametitle{Conclusions \& Further Developments}
    \begin{itemize}
      \item DNS seems to be preferable over classical NS since it produce better results with equal resources. \\
            We expect even better result on a multimodal likelihood.
      \pause
      \item A more accurate analysis on the parameter \(\eta\) in DNS algorithm could be done. Investigate on the optimal value at fixed likelihood calls.
      \pause
      \item As already pointed out in \cite{brewer2011diffusive}, an estimation of errors in DNS can be achieved by considering the static on \(n(j)\) for the \(X_j\),
            and using in-level collected statics for the \(\mathcal{L}_j\).
    \end{itemize}
  \end{frame}


  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  \setbeamertemplate{page number in head/foot}{\phantom{0/0}}
  \begin{frame}[noframenumbering, allowframebreaks]
    \frametitle{Bibliography}
    %\textbf{\structure{Bibliography}}
    \printbibliography
    \vspace{3cm}
  \end{frame}

  \begin{frame}[noframenumbering]
    \centerline{\huge Thanks for the attention!}
    \vfill
    \begin{center}
      \color{blue}{\huge Congrats Italy!\\}
      \vspace{0.3cm}
      \includegraphics[width=0.3\textwidth]{images/euro2020.png}
    \end{center}
  \end{frame}

  \begin{frame}[noframenumbering]
    \frametitle{Information and error estimation}
    Measure of the gained information from prior to posterior:
    \[
      \mathcal{H} \coloneqq \int_\Theta \Post{\theta} \log \frac{\Post{\theta}}{\Prior{\theta}} \diff\theta \quad \xrightarrow[\Post{\theta} = \frac{\Likel{\theta}\Prior{\theta}}{Z}]{\diff X = \Prior{\theta} \diff\theta} \quad
      \mathcal{H} = \int^1_0 \frac{\tLikel{x}}{Z} \log \frac{\tLikel{x}}{Z} \diff x
    \]
    \pause
    \begin{itemize}
      \item The main area of the evidence integral is concentrated around \(X = e^{-\mathcal{H}}\) \cite{skilling2006nested,sivia2006data}.
      \item \(\ExpVal{\log{X_j}} = -\frac{j}{N}\), \(\Var{\left[ X_j\right]} = \frac{j}{N^2}\)
      \item The \(j\)s that contribute most to the evidence are \(\frac{j}{N} \approx \mathcal{H}\).\\
            The uncertainty of \(\log{X_j}\) on that value would be approximately \(\frac{\sqrt{j}}{N} \approx \frac{\sqrt{N\mathcal{H}}}{N} = \sqrt{\frac{\mathcal{H}}{N}}\)
      \item Roughly propagating the error in the \emph{crude formula} we have that the uncertainty on \(\log{Z}\) is
            \[\sqrt{\frac{\mathcal{H}}{N}}\]
      
    \end{itemize}
    
  \end{frame}

%  \begin{frame}[noframenumbering]
%    \frametitle{On MonteCarlo error estimation from \cite{skilling2006nested,sivia2006data}}
%
%  \end{frame}

\end{document}