#include <iostream>
#include <random>
#include <cmath>
#include <array>
#include <algorithm>
#include <cassert>

#include <DiffusiveNestedSampling.cpp>
#include "model.cpp"


int main() {
  const unsigned seed = 647702203;
  const unsigned dimension = 50;
  const double   radius = 45.;

  //const unsigned levels = 14000;
  const unsigned levels = 7000;
  //const unsigned levels = 360;

  const double   regularization = 500.;
  const double   backtracking = 10.;
  const double   enforce = 15.;

  //const double   shrink = 0.975;
  const double   shrink = 0.95;
  //const double   shrink = ns::inv_e<double>;

  const unsigned likelihoods_to_build_level = 10000;
  const unsigned mean_sample_per_level = 1000;
  const unsigned max_likelihoods_calls = 30000000;

  double logZ_realvalue = -ns::hypersphere_log_volume(dimension, radius);
  std::cout << "logZ true value = " << logZ_realvalue << std::endl;

  std::mt19937 rng(seed);
  MultiDimensionalNormal<dimension, double, std::mt19937> mn(radius, rng);

  ns::DiffusiveNestedSampling<MultiDimensionalNormal<dimension, double, std::mt19937>, std::mt19937> dns(mn, rng, levels, regularization, enforce, shrink);

  dns.build_levels(likelihoods_to_build_level, backtracking, true);
  assert(max_likelihoods_calls > mn.likelihood_calls());
  dns.generate_samples(max_likelihoods_calls - mn.likelihood_calls(), true);
  double dns_logZ = dns.log_evidence(true);

  std::clog << "logZ true value = " << logZ_realvalue << std::endl;
  std::clog << "Likelihood calls: " << mn.likelihood_calls() << std::endl;
  std::clog << "DNS logZ estimation: " << dns_logZ << std::endl;
  std::clog << "Relative error " << (dns_logZ - logZ_realvalue)/logZ_realvalue << std::endl;
}