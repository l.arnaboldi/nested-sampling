
#include <iostream>
#include <fstream>
#include <random>
#include <cmath>
#include <array>
#include <iomanip>
#include <set>
#include <utility>

#include <NestedSampling.cpp>
#include "model.cpp"


int main() {
  std::cout << std::fixed << std::setprecision(12);
  const unsigned seed = 220364770;
  const unsigned dimension = 50;
  const double   radius = 45.;
  const unsigned precision_factor = 20000;
  // The numerical factor have been choosen after some tests.
  constexpr const unsigned levels = 1496 * precision_factor;
  constexpr const unsigned alive_particles = 4 * precision_factor;

  double logZ_realvalue = -ns::hypersphere_log_volume(dimension, radius);
  std::cout << "logZ true value = " << logZ_realvalue << std::endl;

  std::mt19937 rng(seed);
  MultiDimensionalNormal<dimension, double, std::mt19937> mn(radius, rng);

  ns::NestedSampling<MultiDimensionalNormal<dimension, double, std::mt19937>> ns(mn, levels, alive_particles);

  ns.run(true);
  std::cout << "Likelihood calls: " << mn.likelihood_calls() << std::endl;
  std::cout << "logZ true value = " << logZ_realvalue << std::endl << std::endl;

  auto crude = ns.crude_log_evidence();
  std::cout << "Crude: " << crude.first << "±" << crude.second << std::endl;
  std::cout << "Distance in uncertainty: " << (crude.first - logZ_realvalue)/crude.second << std::endl;
  std::cout << "Relative error: " << (crude.first - logZ_realvalue)/logZ_realvalue << std::endl;
  std::ofstream crout("part-one-result.txt");
  crout << std::fixed << std::setprecision(12);
  crout << crude.first << " " << crude.second << std::endl;
  crout << (crude.first - logZ_realvalue)/crude.second << std::endl;
  crout << (crude.first - logZ_realvalue)/logZ_realvalue << std::endl;
  crout.close();

  std::set<std::pair<unsigned, std::vector<double>>> mc_results;
  std::vector<unsigned> mc_sample;
  for (unsigned m = 2; m < 50000; m *= 2) mc_sample.push_back(m);
  for (unsigned m = 5; m <= 50; m += 5) mc_sample.push_back(m);

  #pragma omp parallel for
  for (auto m: mc_sample) {
    auto mc = ns.montecarlo_log_evidence(m, rng);
    std::cout << "Montecarlo " << m << ": "  << mc.first << "±" << mc.second;
    std::cout << " | DU: " << (mc.first - logZ_realvalue)/mc.second;
    std::cout << " | RE: " << (mc.first - logZ_realvalue)/logZ_realvalue << std::endl;
    mc_results.insert(std::make_pair(m, std::vector<double>({mc.first, mc.second, (mc.first - logZ_realvalue)/mc.second, (mc.first - logZ_realvalue)/logZ_realvalue})));
  }
  std::ofstream mcout("part-one-mc.txt");
  mcout << std::fixed << std::setprecision(12);
  for (auto& mc: mc_results) {
    mcout << mc.first << ' ';
    for (auto& i: mc.second) {
      mcout << i << ' ';
    }
    mcout << std::endl;
  }
  mcout.close();
  //ns.save_samples("out.txt");
}